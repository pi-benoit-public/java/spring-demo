# Projet en Java avec Spring avec xml

# Description

En java différents tests avec spring et xml.


# Rappel

Télécharger Tomcat.

Télécharger Spring (dist).
Dézipper.

Dans Eclipse, sous J2EE, créer un nouveau projet.

Créer un nouveau dossier "libs" et copier le dossier libs de Spring dans celui-ci.

Puis sur le dossier du projet :

- cliquez sur properties/Java Build Path
- onglet libraries
- add jars
- sélectionner dans le dossier libs les fichiers
- apply/close


## Spring

### IOC  : Inversion Of Control

Délègue à spring la création et le management d'objet.

Il coordonne et exécute l'application.

Sping Development Process pour IOC

1 - Configurer le XML avec les beans.

2 - Créer le spring container.

3 - Récupérer le spring du container.


### DI : Dependency Injection

Permet d'aider la construction de l'objet via Spring.

1 - Constructor Injection

2 - Setter Injection


# Scope

Dans un bean, le scope par défaut est singleton.

scope="singleton" => pour plusieurs objets (?!) pointe sur la même adresse

scope="property" => une adresse différente par objet
