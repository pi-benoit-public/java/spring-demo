package fr.benoit;

public class BaseballCoach implements ICoach {

	IFortuneService ifs;

	public BaseballCoach(IFortuneService ifs) {
		System.out.println("Singleton - Dans le constructor, c'est l'adresse de l'objet dans BaseballCoach = "+ this.hashCode());
		this.ifs = ifs;
	}

	@Override
	public String getDailyWorkout() {
		return "Fais 10 tours de la base.";
	}

	@Override
	public String getDailyFortune() {
		return ifs.getDailyFortune();
	}

}
