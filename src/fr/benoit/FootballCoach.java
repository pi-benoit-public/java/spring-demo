package fr.benoit;

public class FootballCoach implements ICoach {

	IFortuneService ifs;

	public FootballCoach(IFortuneService ifs) {
		System.out.println("Prototype - Dans le constructor, c'est l'adresse de l'objet dans FootballCoach = "+ this.hashCode());
		this.ifs = ifs;
	}

	@Override
	public String getDailyWorkout() {
		return "Cours Forrest avec ton ballon.";
	}

	@Override
	public String getDailyFortune() {
		return ifs.getDailyFortune();
	}

}
