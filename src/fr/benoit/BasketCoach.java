package fr.benoit;

import javax.swing.JOptionPane;

public class BasketCoach implements ICoach {

	String email;
	String team;
	IFortuneService ifs;

	public void myInitMethod() {
		JOptionPane.showMessageDialog(null, "BasketCoach - appel de la m�thode init.");
	}

	public void myDestroyMethod() {
		JOptionPane.showMessageDialog(null, "BasketCoach - appel de la m�thode � la fermeture du bean.");
	}

	@Override
	public String getDailyWorkout() {
		return "Fais 3 paniers � 3 points.";
	}

	@Override
	public String getDailyFortune() {
		return ifs.getDailyFortune();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public IFortuneService getIfs() {
		return ifs;
	}

	public void setIfs(IFortuneService ifs) {
		this.ifs = ifs;
	}

}
