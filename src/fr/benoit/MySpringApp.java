package fr.benoit;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MySpringApp {

	public static void main(String[] args) {

		// Avec Spring.
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		ICoach cbb = context.getBean("coachBaseball", ICoach.class);
		ICoach cbb2 = context.getBean("coachBaseball", ICoach.class);

		ICoach cfooteux = context.getBean("coachFootball", ICoach.class);
		ICoach cfooteux2 = context.getBean("coachFootball", ICoach.class);

		ICoach cbasket = context.getBean("coachBasket", ICoach.class);

		System.out.println(cbb.getDailyWorkout());
//		System.out.println(cbb2.getDailyWorkout());

		System.out.println(cfooteux.getDailyWorkout());
		System.out.println(cbasket.getDailyWorkout());

		System.out.println("cbb=>"+ cbb.getDailyFortune());
		System.out.println("cfooteux=>"+ cfooteux.getDailyFortune());
		System.out.println("cbasket=>"+ cbasket.getDailyFortune());

		BasketCoach cbasket2 = context.getBean("coachBasket", BasketCoach.class);
		System.out.println(cbasket2.getEmail());
		System.out.println(cbasket2.getTeam());

		context.close();

	}

}
